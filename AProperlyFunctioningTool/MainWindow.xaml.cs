﻿using AProperlyFunctioningTool.Models;
using AProperlyFunctioningTool.ViewModels;
using Ext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AProperlyFunctioningTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Dictionary<string, int> data;
        DropStack<TallyItemModel> undoStack;

        TallyItemViewModel viewModel { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            data = new Dictionary<string, int>();
            undoStack = new DropStack<TallyItemModel>(22);
            viewModel = new TallyItemViewModel();
            this.DataContext = viewModel;
        }

        private void increment_Click(object sender, RoutedEventArgs e)
        {

            TallyItemModel tim = ((TallyItem)sender).Tag as TallyItemModel;
            tim.Count++;
            undoStack.Push(tim);
            Total();
        }

        private void Undobtn_Click(object sender, RoutedEventArgs e)
        {
            if (undoStack.Count > 0)
            {
                var item = undoStack.Pop();
                item.Count--;
                Total();
            }
        }

        private void Copybtn_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in viewModel.GetTallyList())
            {
                if (dataOnlyChk.IsChecked ?? false)
                    sb.AppendLine($"{item.Count}");
                else
                    sb.AppendLine($"{item.Count}\t{item.Name}");
            }
            sb.Append("\n");
            sb.Append(viewModel.Total);
            Clipboard.SetText(sb.ToString());
        }

        private void Total()
        {
            int t = 0;
            foreach (var item in viewModel.GetTallyList())
            {
                t += item.Count;
            }
            viewModel.Total = t;
            totalLabel.Content = $"Total: {viewModel.Total}";
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var a = MessageBox.Show("Are you sure?", "Bye Bye Data", MessageBoxButton.YesNo);
            if(a == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ItemLst_Loaded(object sender, RoutedEventArgs e)
        {
            viewModel.LoadTallyItems(Properties.Settings.Default.DefaultProfile);
            itemLst.DataContext = viewModel;

        }

        private void Config_Click(object sender, RoutedEventArgs e)
        {
            Config window = new Config();
            if (window.ShowDialog()  == true){
                viewModel = new TallyItemViewModel();
                viewModel.LoadTallyItems(Properties.Settings.Default.DefaultProfile);
                itemLst.DataContext = viewModel;
            }
        }
    }
}
