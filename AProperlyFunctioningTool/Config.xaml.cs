﻿using AProperlyFunctioningTool.Common;
using AProperlyFunctioningTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AProperlyFunctioningTool
{
    /// <summary>
    /// Interaction logic for Config.xaml
    /// </summary>
    public partial class Config : Window
    {
        ConfigViewModel cvm;
        string defaultTxt = "default.txt";
        string auditorTxt = "auditor.txt";

        public Config()
        {
            InitializeComponent();
            cvm = new ConfigViewModel();
            cvm.LoadData();
            DataContext = cvm;
            SetCheckBox();
        }


        private void UpBtn_Click(object sender, RoutedEventArgs e)
        {
            int selected = itemsBox.SelectedIndex;
            if(selected > 0)
            {
                cvm.FileContent.Insert(selected - 1, cvm.FileContent[selected]);
                cvm.FileContent.RemoveAt(selected + 1);
                itemsBox.SelectedIndex = selected - 1;
            }
        }

        private void DownBtn_Click(object sender, RoutedEventArgs e)
        {
            int selected = itemsBox.SelectedIndex;
            if (selected < itemsBox.Items.Count - 1 & selected != -1)
            {
                cvm.FileContent.Insert(selected + 2, cvm.FileContent[selected]);
                cvm.FileContent.RemoveAt(selected);
                itemsBox.SelectedIndex = selected + 1;

            }
        }

        private void FilesCBx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cvm.LoadContent();
           
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (firstPassrb.IsChecked ?? false)
            {
                Properties.Settings.Default.DefaultProfile = "default.txt";
            }else if(auditorrb.IsChecked ?? false)
            {
                Properties.Settings.Default.DefaultProfile = "auditor.txt";
            }
            else if(selectedrb.IsChecked ?? false)
            {
                Properties.Settings.Default.DefaultProfile = cvm.SelectedFile;
            }
            Properties.Settings.Default.Save();
            Saver.SaveItems(cvm.FileContent.ToList(), cvm.SelectedFile);
            DialogResult = true;
        }

        private void AddItemBtn_Click(object sender, RoutedEventArgs e)
        {
            var textDiag = new TextDialog();
            if(textDiag.ShowDialog() == true)
            {
                cvm.FileContent.Add(textDiag.ResponseText.Replace(",", ""));
            }
        }

        private void SaveProfilebtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Saver.SaveItems(cvm.FileContent.ToList(), cvm.SelectedFile);
            }catch(Exception ex)
            {
                MessageBox.Show("Error Saving Profile.");
            }
        }

        private void NewProfileBtn_Click(object sender, RoutedEventArgs e)
        {
            var textDiag = new TextDialog();
            if (textDiag.ShowDialog() == true)
            {
                string a = textDiag.ResponseText;
                cvm.FileNames.Add(a + ".txt");
                cvm.SelectedFile=a + ".txt";
                filesCBx.SelectedItem = a + ".txt";
                cvm.LoadContent();
            }
        }

        private void SetCheckBox()
        {
            if (Properties.Settings.Default.DefaultProfile == defaultTxt)
            {
                firstPassrb.IsChecked = true;
            }
            else if (Properties.Settings.Default.DefaultProfile == auditorTxt)
            {
                auditorrb.IsChecked = true;
            }
            else
            {
                selectedrb.IsChecked = true;
            }
        }
    }

  
}
