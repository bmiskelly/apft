﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AProperlyFunctioningTool.Models
{
    public class TallyItemModel : INotifyPropertyChanged
    {
        private string name;
        private int count;

        public string Name {
            get {
                return name;
            }
            set {
                if (name != value)
                {
                    name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }
        public int Count {
            get {
                return count;
            }
            set {
                if (count != value)
                {
                    count = value;
                    RaisePropertyChanged("Count");
                }
            }
        }

        public TallyItemModel(string name)
        {
            Name = name;
            Count = 0;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
