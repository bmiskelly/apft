﻿using AProperlyFunctioningTool.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AProperlyFunctioningTool.ViewModels
{
    public class ConfigViewModel
    {

        private ObservableCollection<string> _fileNames;
        public ObservableCollection<string> FileNames {
            get {
                if (_fileNames == null)
                {
                    _fileNames = new ObservableCollection<string>();
                    return _fileNames;
                }
                else
                {
                    return _fileNames;
                }
            }
            set {
                _fileNames = value;
            }
        }

        private ObservableCollection<string> _fileContent;
        public ObservableCollection<string> FileContent {
            get {
                if (_fileContent == null)
                {
                    _fileContent = new ObservableCollection<string>();
                    return _fileContent;
                }
                else
                {
                    return _fileContent;
                }
            }
            set {
                _fileContent = value;
            }
        }

        public string SelectedFile { get; set; }

        public void LoadData()
        {
            var filenames = Saver.GetFileNames();
            foreach (var name in filenames)
            {
                var nameonly = name.Split('\\').Last();
                FileNames.Add(nameonly);
            }
            SelectedFile = Properties.Settings.Default.DefaultProfile;
            LoadContent();
        }

        public void LoadContent()
        {
            FileContent.Clear();
            var contentList = Saver.GetItems(SelectedFile);
            foreach (var item in contentList)
            {
                FileContent.Add(item);
            }
        }
    }

    public class FileData
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public FileData(string path, string name)
        {
            Path = path;
            Name = name;
        }
    }
}
