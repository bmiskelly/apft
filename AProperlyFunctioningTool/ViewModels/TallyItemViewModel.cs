﻿using AProperlyFunctioningTool.Common;
using AProperlyFunctioningTool.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AProperlyFunctioningTool.ViewModels
{
    public class TallyItemViewModel
    {
        public ObservableCollection<TallyItemModel> TallyItemList1 { get; set; }
        public ObservableCollection<TallyItemModel> TallyItemList2 { get; set; }
        public int Total { get; set; }

        public void LoadTallyItems(string fileName)
        {
            
            ObservableCollection<TallyItemModel> tallyItems = new ObservableCollection<TallyItemModel>();
            ObservableCollection<TallyItemModel> tallyItems2 = new ObservableCollection<TallyItemModel>();

            var listFromFile = Saver.GetItems(fileName);

            if (listFromFile.Count > 0)
            {
                int halfcount = listFromFile.Count / 2;
                if (listFromFile.Count % 2 != 0)
                    halfcount++;
                var list1 = listFromFile.Take(halfcount);
                var list2 = listFromFile.Skip(halfcount);

                foreach (var item in list1)
                {
                    tallyItems.Add(new TallyItemModel(item));
                }
                foreach (var item in list2)
                {
                    tallyItems2.Add(new TallyItemModel(item));
                }
            }

            TallyItemList1 = tallyItems;
            TallyItemList2 = tallyItems2;
            
        }

        public List<TallyItemModel> GetTallyList()
        {
            List<TallyItemModel> list = new List<TallyItemModel>();
            list.AddRange(TallyItemList1);
            list.AddRange(TallyItemList2);
            return list;
        }
    }


}
