﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AProperlyFunctioningTool
{
    /// <summary>
    /// Interaction logic for TallyItem.xaml
    /// </summary>
    public partial class TallyItem : UserControl
    {
        public static readonly DependencyProperty ItemNameProperty = DependencyProperty.Register("ItemName", typeof(string), typeof(TallyItem));
        public static readonly DependencyProperty CountNameProperty = DependencyProperty.Register("Count", typeof(int), typeof(TallyItem));

        
        public event RoutedEventHandler Click;

        public string ItemName 
        {
            get { return (string)GetValue(ItemNameProperty); }
            set { SetValue(ItemNameProperty, value); }
        }

        public int Count {
            get { return (int)GetValue(CountNameProperty); }
            set { SetValue(CountNameProperty, value); }
        }

        public TallyItem()
        {
            InitializeComponent();
        }

        void onButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.Click != null)
            {
                this.Click(this, e);
            }
        }
    }
}
