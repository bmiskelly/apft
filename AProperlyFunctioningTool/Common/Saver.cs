﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AProperlyFunctioningTool.Common
{
    public static class Saver
    {
        private static string configPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "configs");
        private static string defaultLoad = "default";
        private static string ext = ".txt";

        public static List<string> GetFileNames()
        {
            return Directory.GetFiles(configPath).ToList();
        }

        public static List<string> GetItems(string filename)
        {
            List<string> itemNames = new List<string>();
            string filePath = Path.Combine(configPath, filename);
            if (File.Exists(filePath))
            {
                string text = File.ReadAllText(filePath);
                itemNames = text.Split(',').ToList();
            }

            return itemNames;
        }

        public static void SaveItems(List<string> itemNames, string fileName)
        {
            string text = string.Join(",", itemNames);
            string path = Path.Combine(configPath, fileName);

            File.WriteAllText(path, text);
        }
    }
}
